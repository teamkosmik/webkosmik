<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {		

	// Main Page Member
	public function index() {
		
		$site  		= $this->mConfig->list_config();
		$blogs		= $this->mBlogs->listBlogsPub();
		
		// Validasi
		$valid = $this->form_validation;
		$valid->set_rules('nim','Nim','required');
		$valid->set_rules('nama','Nama','required');
		$valid->set_rules('email','Email','required');
		$valid->set_rules('no_hp','Nomor Hp','required');
		$valid->set_rules('divisi','Divisi Yang Dipilih','required');
		$valid->set_rules('alasan','Alasan Masuk Kosmik','required');
		
		if($valid->run() === FALSE) {
		
		$data = array(	'title'	=> 'Daftar Member - '.$site['nameweb'],
						'site'	=> $site,
						'blogs'	=> $blogs,
						'isi'	=> 'front/member/list');
		$this->load->view('front/layout/wrapper',$data);
		}else{

			$i = $this->input;
			$slugMember = url_title($this->input->post('nama'), 'dash', TRUE);
			$data = array(	'slug_member'	=> $slugMember,
							'nim'		=> $i->post('nim'),	
							'nama'		=> $i->post('nama'),
							'email'		=> $i->post('email'),
							'no_hp'		=> $i->post('no_hp'),
							'divisi'	=> $i->post('divisi'),			
							'alat_musik'=> $i->post('alat_musik'),
							'alasan'	=> $i->post('alasan'),
						);
			$this->mMember->createMember($data);		
			$this->session->set_flashdata('sukses','Success');
			redirect(base_url('member'));
		}
	}	
}
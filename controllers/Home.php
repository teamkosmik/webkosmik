<?php
	/*
    @Copyright Indra Rukmana
    @Class Name : Home(Front)
	*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	// Main Page Home
	public function index() {

		$site  		= $this->mConfig->list_config();
		$sliders     = $this->mSlider->listSlider();
		$blogs  	= $this->mBlogs->listBlogsPub();
		
		$data = array(	'title'		=> 'Home - '.$site['nameweb'],
						'site'		=> $site,
						'sliders'	=> $sliders,
						'blogs'		=> $blogs,
						'isi'		=> 'front/home/list');
		$this->load->view('front/layout/wrapper',$data);
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
	
	// Main Page Member
	public function index() {

		$site  	  = $this->mConfig->list_config();
		$member = $this->mMember->listMember();
		
		$data = array(	'title'		=> 'List Member - '.$site['nameweb'],
						'member'	=> $member,
						'isi'		=> 'admin/member/list');
		$this->load->view('admin/layout/wrapper',$data);
	}

	// Create Member
	public function create() {
		
		$site = $this->mConfig->list_config();
		
		$v = $this->form_validation;
		$v->set_rules('nama','Nama','required');
		
		if($v->run()) {
				$i = $this->input;
				$slugMember = url_title($this->input->post('nama'), 'dash', TRUE);
				$data = array(	'slug_member'	=> $slugMember,
								'nim'			=> $i->post('nim'),
								'nama'			=> $i->post('nama'),
								'email'			=> $i->post('email'),
								'no_hp'			=> $i->post('no_hp'),
								'divisi'		=> $i->post('divisi'),	
								'alat_musik'	=> $i->post('alat_musik'),					
								'alasan'		=> $i->post('alasan')
				 			 );

				$this->mMember->createMember($data);
				$this->session->set_flashdata('sukses','Success');
				redirect(base_url('admin/member/'));
		}
		// Default page
		$data = array(	'title'		=> 'Create Member - '.$site['nameweb'],
						'site'		=> $site,
						'isi'		=> 'admin/member/create');
		$this->load->view('admin/layout/wrapper',$data);
	}

	// Edit Product
	public function edit($member_id) {

		$member	= $this->mMember->detailMember($member_id);
		$endMember	= $this->mMember->endMember();		

		// Validation
		$v = $this->form_validation;
		$v->set_rules('nama','Nama','required');
		
		if($v->run()) {
		
			$i = $this->input;
			$slugMember = $endMember['member_id'].'-'.url_title($i->post('nama'),'dash', TRUE);
			$data = array(	'member_id'		=> $member['member_id'],
							'slug_member'	=> $slugmember,
							'nim'			=> $i->post('nim'),
							'nama'			=> $i->post('nama'),
							'email'			=> $i->post('email'),
							'no_hp'			=> $i->post('no_hp'),
							'divisi'		=> $i->post('divisi'),	
							'alat_musik'	=> $i->post('alat_musik'),					
							'alasan'		=> $i->post('alasan')
							);
			$this->mMember->editMember($data);
			$this->session->set_flashdata('sukses','Success');
			redirect(base_url('admin/member'));			
		}

		$data = array(	'title'		=> 'Edit Member - '.$member['nama'],
						'member'	=> $member,
						'isi'		=> 'admin/member/edit');
		$this->load->view('admin/layout/wrapper', $data);
	}

	// Delete Product
	public function delete($member_id) {
		$data = array('member_id'	=> $member_id);
		$this->mMember->deleteMember($data);		
		$this->session->set_flashdata('sukses','Success');
		redirect(base_url('admin/member'));
	}		
}
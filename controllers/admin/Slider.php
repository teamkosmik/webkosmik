<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
	
	// Main Page Products
	public function index() {

		$site      	= $this->mConfig->list_config();
		$slider 	= $this->mSlider->listSlider();
		
		$data = array(	'title'		=> 'Management Slider - '.$site['nameweb'],
						'slider'	=> $slider,
						'site'		=> $site,
						'isi'		=> 'admin/slider/list');
		$this->load->view('admin/layout/wrapper',$data);
	}

	// Create Slider
	public function create() {
		
		$site = $this->mConfig->list_config();
		
		$v = $this->form_validation;
		$v->set_rules('slider_name','Slider Name','required');
		
		if($v->run()) {
			
			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_size']			= '10000'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('image')) {
				
		$data = array(	'title'			=> 'Create Slider - '.$site['nameweb'],
						'site'			=> $site,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/slider/create');
		$this->load->view('admin/layout/wrapper',$data);
		}else{
				$upload_data				= array('uploads' =>$this->upload->data());
				$config['image_library']	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
				$config['new_image'] 		= './assets/upload/image/thumbs/';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width'] 			= 150; // Pixel
				$config['height'] 			= 150; // Pixel
				$config['thumb_marker'] 	= '';
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$i = $this->input;
				$slugSlider = url_title($this->input->post('slider_name'), 'dash', TRUE);
				$data = array(	'user_id'		=> $this->session->userdata('id'),
								'slider_name'	=> $i->post('slider_name'),	
								'slug_slider'	=> $slugSlider,
								'image'			=> $upload_data['uploads']['file_name'],
								'description' 	=> $i->post('description')
				 			 );
				$this->mSlider->createSlider($data);
				$this->session->set_flashdata('sukses','Success');
				redirect(base_url('admin/slider/'));
		}}
		// Default page
		$data = array(	'title'		=> 'Create Slider - '.$site['nameweb'],
						'site'		=> $site,
						'isi'		=> 'admin/slider/create');
		$this->load->view('admin/layout/wrapper',$data);
	}

	// Edit Slider
	public function edit($slider_id) {

		$slider	= $this->mSlider->detailSlider($slider_id);
		$endSlider	= $this->mSlider->endSlider();		

		// Validation
		$v = $this->form_validation;
		$v->set_rules('slider_name','Slider Name','required');
		
		if($v->run()) {
			if(!empty($_FILES['image']['name'])) {
			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png|svg';
			$config['max_size']			= '10000'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('image')) {
		
		$data = array(	'title'		=> 'Edit Slider - '.$slider['slider_name'],
						'slider'	=> $slider,
						'error'		=> $this->upload->display_errors(),
						'isi'		=> 'admin/slider/edit');
		$this->load->view('admin/layout/wrapper', $data);
		}else{
				$upload_data				= array('uploads' =>$this->upload->data());
				$config['image_library']	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
				$config['new_image'] 		= './assets/upload/image/thumbs/';
				$config['create_thumb'] 	= TRUE;
				$config['quality'] 			= "100%";
				$config['maintain_ratio'] 	= FALSE;
				$config['width'] 			= 360; // Pixel
				$config['height'] 			= 200; // Pixel
				$config['x_axis'] 			= 0;
				$config['y_axis'] 			= 0;
				$config['thumb_marker'] 	= '';
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				
			$i = $this->input;

			unlink('./assets/upload/image/'.$slider['image']);
			unlink('./assets/upload/image/thumbs/'.$slider['image']);

			$slugSldier = $endSlider['slider_id'].'-'.url_title($i->post('slider_name'),'dash', TRUE);
			$data = array(	'slider_id'		=> $slider['slider_id'],
							'user_id'		=> $this->session->userdata('id'),
							'slider_name'	=> $i->post('slider_name'),
							'slug_slider'	=> $slugSldier,
							'image'			=> $upload_data['uploads']['file_name'],
							'description' 	=> $i->post('description')
							);
			$this->mSlider->editSlider($data);
			$this->session->set_flashdata('sukses','Success');
			redirect(base_url('admin/slider'));
		}}else{
			$i = $this->input;
			$slugSlider = $endSlider['slider_id'].'-'.url_title($i->post('slider_name'),'dash', TRUE);
			$data = array(	'slider_id'		=> $slider['slider_id'],
							'user_id'		=> $this->session->userdata('id'),
							'slider_name'	=> $i->post('slider_name'),
							'slug_slider'	=> $slugSlider,
							'description'	=> $i->post('description'),
							);
			$this->mSlider->editSlider($data);
			$this->session->set_flashdata('sukses','Success');
			redirect(base_url('admin/slider'));			
		}}

		$data = array(	'title'		=> 'Edit Slider - '.$slider['slider_name'],
						'slider'	=> $slider,
						'isi'		=> 'admin/slider/edit');
		$this->load->view('admin/layout/wrapper', $data);
	}	

	// Delete Slider
	public function delete($slider_id) {
		$data = array('slider_id'	=> $slider_id);
		$this->mSlider->deleteSlider($data);		
		$this->session->set_flashdata('sukses','Success');
		redirect(base_url('admin/slider'));
	}	
}
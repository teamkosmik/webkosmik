<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	// Main Page Profile
	public function index() {

		$site  	  = $this->mConfig->list_config();
		$profile = $this->mProfile->listProfile();
		
		$data = array(	'title'		=> 'List Profile - '.$site['nameweb'],
						'profile'	=> $profile,
						'isi'		=> 'admin/profile/list');
		$this->load->view('admin/layout/wrapper',$data);
	}

	// Create Profile
	public function create() {
		
		$site = $this->mConfig->list_config();
		
		$v = $this->form_validation;
		$v->set_rules('description','Description','required');
		
		if($v->run()) {
			
			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_size']			= '10000'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('image')) {
				
		$data = array(	'title'			=> 'Create Profile - '.$site['nameweb'],
						'site'			=> $site,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/profile/create');
		$this->load->view('admin/layout/wrapper',$data);
		}else{
				$upload_data				= array('uploads' =>$this->upload->data());
				$config['image_library']	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
				$config['new_image'] 		= './assets/upload/image/thumbs/';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width'] 			= 150; // Pixel
				$config['height'] 			= 150; // Pixel
				$config['thumb_marker'] 	= '';
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$i = $this->input;
				$slugProfile = url_title($this->input->post('name'), 'dash', TRUE);
				$data = array(	'user_id'		=> $this->session->userdata('id'),
								'slug_profile'	=> $slugProfile,
								'name'			=> $i->post('name'),
								'image'			=> $upload_data['uploads']['file_name'],
								'description'	=> $i->post('description')
				 			 );

				$this->mProfile->createProfile($data);
				$this->session->set_flashdata('sukses','Success');
				redirect(base_url('admin/profile/'));
		}}
		// Default page
		$data = array(	'title'		=> 'Create Profile - '.$site['nameweb'],
						'site'		=> $site,
						'isi'		=> 'admin/profile/create');
		$this->load->view('admin/layout/wrapper',$data);
	}

	// Edit Profile
	public function edit($profile_id) {

		$profile	= $this->mProfile->detailProfile($profile_id);
		$endProfile	= $this->mProfile->endProfile();		

		// Validation
		$v = $this->form_validation;
		$v->set_rules('name','Name','required');
		
		if($v->run()) {
			if(!empty($_FILES['image']['name'])) {
			$config['upload_path'] 		= './assets/upload/image/';
			$config['allowed_types'] 	= 'gif|jpg|png|svg';
			$config['max_size']			= '10000'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('image')) {
		
		$data = array(	'title'		=> 'Edit Profile - '.$product['name'],
						'profile'	=> $profile,
						'error'		=> $this->upload->display_errors(),
						'isi'		=> 'admin/profile/edit');
			$this->load->view('admin/layout/wrapper', $data);
			}else{
					$upload_data				= array('uploads' =>$this->upload->data());
					$config['image_library']	= 'gd2';
					$config['source_image'] 	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
					$config['new_image'] 		= './assets/upload/image/thumbs/';
					$config['create_thumb'] 	= TRUE;
					$config['quality'] 			= "100%";
					$config['maintain_ratio'] 	= FALSE;
					$config['width'] 			= 360; // Pixel
					$config['height'] 			= 200; // Pixel
					$config['x_axis'] 			= 0;
					$config['y_axis'] 			= 0;
					$config['thumb_marker'] 	= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					
				$i = $this->input;

				unlink('./assets/upload/image/'.$profile['image']);
				unlink('./assets/upload/image/thumbs/'.$profile['image']);

				$slugProfile = $endProfile['profile_id'].'-'.url_title($i->post('name'),'dash', TRUE);
				$data = array(	'profile_id'	=> $profile['profile_id'],
								'user_id'		=> $this->session->userdata('id'),
								'slug_profile'	=> $slugProfile,
								'name'			=> $i->post('name'),
								'image'			=> $upload_data['uploads']['file_name'],
								'description'	=> $i->post('description'),
								);
				$this->mProfile->editProfile($data);
				$this->session->set_flashdata('sukses','Success');
				redirect(base_url('admin/profile'));
		}}else{
			$i = $this->input;
			$slugProfile = $endProfile['profile_id'].'-'.url_title($i->post('name'),'dash', TRUE);
			$data = array(	'profile_id'	=> $profile['profile_id'],
							'user_id'		=> $this->session->userdata('id'),
							'slug_profile'	=> $slugProfile,
							'name'			=> $i->post('name'),
							'description'	=> $i->post('description'),
							);
			$this->mProfile->editProfile($data);
			$this->session->set_flashdata('sukses','Success');
			redirect(base_url('admin/Profile'));			
		}}

		$data = array(	'title'		=> 'Edit Profile - '.$profile['name'],
						'profile'	=> $profile,
						'isi'		=> 'admin/profile/edit');
		$this->load->view('admin/layout/wrapper', $data);
	}	

	// Delete Profile
	public function delete($profile_id) {
		$data = array('profile_id'	=> $profile_id);
		$this->mProfile->deleteProfile($data);		
		$this->session->set_flashdata('sukses','Success');
		redirect(base_url('admin/profile'));
	}		
}
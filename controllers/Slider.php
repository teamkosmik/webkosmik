<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

	public function index() {

		$site  		= $this->mConfig->list_config();
		$slider 	= $this->mSlider->listSlider();
		
		$data = array(	'title'		=> 'Slider Image - '.$site['nameweb'],
						'site'		=> $site,
						'slider'	=> $slider,
						'countSlider' => $this->mStats->slider(),						
						'isi'		=> 'front/slider/list');
		$this->load->view('front/layout/wrapper',$data);
	}
}
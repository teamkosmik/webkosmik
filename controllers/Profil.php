<?php
	/*
    @Copyright Indra Rukmana
    @Class Name : Profil(Front)
	*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
	
	// Main Page Profile
	public function index() {

		$site  		= $this->mConfig->list_config();
		$blogs		= $this->mBlogs->listBlogsPub();
		$profile    = $this->mProfile->listProfile();
		
		$data = array(	'title'		=> 'Profil - '.$site['nameweb'],
						'site'		=> $site,
						'blogs'		=> $blogs,
						'profile'	=> $profile,
						'isi'		=> 'front/profile/list');
		$this->load->view('front/layout/wrapper',$data);
	}	
}
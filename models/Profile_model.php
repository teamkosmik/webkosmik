<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Profile_model extends CI_Model {

        public function __construct() {
            $this->load->database();
        }

        // Listing Profile
        public function listProfile() {
            $this->db->select('*');
            $this->db->from('profile'); 
            $this->db->join('admins','admins.admin_id = profile.user_id','LEFT');
            $this->db->where(array('profile_id' => '1'));                         
            $this->db->order_by('profile_id','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }       

        // Create Product
        public function createProfile($data) {
            $this->db->insert('profile',$data);
        }

        // Detail Profile
        public function detailProfile($profile_id) {
            $this->db->select('*');
            $this->db->from('profile');
            $this->db->where('profile_id',$profile_id);
            $this->db->order_by('profile_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }

        // Read Product
        public function readProfile($slugProfile) {
            $this->db->select('*');
            $this->db->from('profile');
            $this->db->where('slug_profile',$slugProfile);
            $query = $this->db->get();
            return $query->row_array();
        }         

        // Edit Profile
        public function editProfile($data) {
            $this->db->where('profile_id',$data['profile_id']);
            $this->db->update('profile',$data);
        }           

        // Delete Profile
        public function deleteProfile($data) {
            $this->db->where('profile_id',$data['profile_id']);
            $this->db->delete('profile',$data);
        } 

        // End Profile
        public function endProfile() {
            $this->db->select('*');
            $this->db->from('profile');
            $this->db->order_by('profile_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }                         
    }

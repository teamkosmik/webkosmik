<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Galleries_model extends CI_Model {

        public function __construct() {
            $this->load->database();
        }

        // Listing Gallery
        public function listGalleries() {
            $this->db->select('*');
            $this->db->from('galleries');
            $this->db->join('admins','admins.admin_id = galleries.user_id','LEFT');                        
            $this->db->order_by('gallery_id','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }

        // Listing Galleries Publish
        public function listGalleriesPub() {
            $this->db->select('*');
            $this->db->from('galleries');
            $this->db->join('admins','admins.admin_id = galleries.user_id','LEFT');
            $this->db->where(array('status' => 'publish'));                        
            $this->db->order_by('gallery_id','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }                    

        // Create Gallery
        public function createGallery($data) {
            $this->db->insert('galleries',$data);
        }

        // Detail Gallery
        public function detailGallery($gallery_id) {
            $this->db->select('*');
            $this->db->from('galleries');
            $this->db->where('gallery_id',$gallery_id);
            $this->db->order_by('gallery_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        } 

        // Edit Gallery
        public function editGallery($data) {
            $this->db->where('gallery_id',$data['gallery_id']);
            $this->db->update('galleries',$data);
        }           

        // Delete Gallery
        public function deleteGallery($data) {
            $this->db->where('gallery_id',$data['gallery_id']);
            $this->db->delete('galleries',$data);
        }        

        // End Gallery
        public function endGallery() {
            $this->db->select('*');
            $this->db->from('galleries');
            $this->db->order_by('gallery_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }              

    }

<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Member_model extends CI_Model {

        public function __construct() {
            $this->load->database();
        }

        // Listing Member
        public function listMember() {
            $this->db->select('*');
            $this->db->from('member');                     
            $this->db->order_by('member_id','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }    

        // Read Member
        public function readMember($slugMember) {
            $this->db->select('*');
            $this->db->from('member');
            $this->db->where('slug_member',$slugMember);
            $query = $this->db->get();
            return $query->row_array();
        }  

        // Create Member
        public function createMember($data) {
            $this->db->insert('member',$data);
        }

        // Detail Member
        public function detailMember($member_id) {
            $this->db->select('*');
            $this->db->from('member');
            $this->db->where('member_id',$member_id);
            $this->db->order_by('member_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }
    
        // Edit Member
        public function editMember($data) {
            $this->db->where('member_id',$data['member_id']);
            $this->db->update('member',$data);
        }           

        // Delete Member
        public function deleteMember($data) {
            $this->db->where('member_id',$data['member_id']);
            $this->db->delete('member',$data);
        } 

        // End Member
        public function endMember() {
            $this->db->select('*');
            $this->db->from('member');
            $this->db->order_by('member_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }  

        // Per Page Member
        public function perPageMember($limit,$start) {
            $this->db->select('*');
            $this->db->from('member');           
            $this->db->order_by('member_id','ASC');
            $this->db->limit($limit,$start);
            $query = $this->db->get();
            return $query->result_array();
        } 

        // Total Member
        public function totalMember() {
            $this->db->select('*');
            $this->db->from('member');              
            $this->db->order_by('member_id','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }    
    }

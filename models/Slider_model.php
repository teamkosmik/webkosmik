<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Slider_model extends CI_Model {

        public function __construct() {
            $this->load->database();
        }

        // Listing Slider
        public function listSlider() {
            $this->db->select('*');
            $this->db->from('slider');
            $this->db->join('admins','admins.admin_id = slider.user_id','LEFT');                        
            $this->db->order_by('slider_id','ASC');
            $query = $this->db->get();
            return $query->result_array();
        }
                             

        // Create Slider
        public function createSlider($data) {
            $this->db->insert('slider',$data);
        }

        // Detail Slider
        public function detailSlider($slider_id) {
            $this->db->select('*');
            $this->db->from('slider');
            $this->db->where('slider_id',$slider_id);
            $this->db->order_by('slider_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        } 

        // Edit Slider
        public function editSlider($data) {
            $this->db->where('slider_id',$data['slider_id']);
            $this->db->update('slider',$data);
        }           

        // Delete Slider
        public function deleteSlider($data) {
            $this->db->where('slider_id',$data['slider_id']);
            $this->db->delete('slider',$data);
        }        

        // End Slider
        public function endSlider() {
            $this->db->select('*');
            $this->db->from('slider');
            $this->db->order_by('slider_id','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }              

    }

    <div id="k-body"><!-- content wrapper -->
    
        <div class="container"><!-- container -->
        
            <div class="row"><!-- row -->            
            
                <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->
                
                    <br>
                </div><!-- breadcrumbs end -->
                
            </div><!-- row end -->
            
            <?php include('slider.php');?>
            
            <div class="row no-gutter"><!-- row -->
                <br>
                <?php include ('news.php');?>
                <?php include ('video.php');?>         
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- content wrapper end -->
  
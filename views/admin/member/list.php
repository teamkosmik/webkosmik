<?php
// Session 
if($this->session->flashdata('sukses')) { 
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('sukses');
	echo '</div>';
} 
// Error
echo validation_errors('<div class="alert alert-success">','</div>'); 
?>

<!--  Modals-->
<div class="panel-body">
<p><a href="<?php echo base_url('admin/member/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Create New Member</a></p>



<table class="table table-striped table-bordered table-hover" id="dataTables-example">
<thead>
    <tr>
        <th>#</th>
        <th>Nim</th>
        <th>Nama</th>
        <th>Divisi</th>
        <th width="150px">Action</th>
    </tr>
</thead>
<tbody>
	<?php $i=1; foreach($member as $list) { ?>
    <tr class="odd gradeX">
        <td><?php echo $i; ?></td>
        <td><?php echo $list['nim'] ?></td>
        <td><?php echo $list['nama'] ?></td>
        <td><?php echo $list['divisi'] ?></td>     
        <td class="center">
        <a href="<?php echo base_url('admin/member/edit/'.$list['member_id']);?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
       <!-- View Biz -->
       <!--  Modals-->
        <button class="btn btn-success" data-toggle="modal" data-target="#View<?php echo $list['member_id']; ?>"><i class="fa fa-eye"></i></button>

        <div class="modal fade" id="View<?php echo $list['member_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">View Post</h4>
              </div>
              <div class="modal-body">
              <div class="col-md-12">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered table-hover">
          <tr>
            <td>NIM</td>
            <td><?php echo $list['nim'] ?></td>
          </tr>
          <tr>
            <td>Nama</td>
            <td><?php echo $list['nama'] ?></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><?php echo $list['email'] ?></td>
          </tr>
          <tr>
            <td>No Hp</td>
            <td><?php echo $list['no_hp'] ?></td>
          </tr>
          <tr>
            <td>Divisi</td>
            <td><?php echo $list['divisi'] ?></td>
          </tr>
          <tr>
            <td>Alat Musik</td>
            <td><?php echo $list['alat_musik'] ?></td>
          </tr>                
          <tr>
                <td colspan="2">Alasan Masuk Kosmik : <?php echo $list['alasan'];?></td>
          </tr>                   
          <tr>
            <td>&nbsp;</td>
            <td>
            <a href="<?php echo base_url('admin/member/edit/'.$list['member_id']) ?>" class="btn btn-primary">Edit</a>
            <a href="<?php echo base_url('admin/member/delete/'.$list['member_id']) ?>" class="btn btn-danger">Delete</a>
          </tr>
        </table>
        </div>
        <div class="clearfix"></div>
              </div>
              
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
        </div>
        <!-- End Modals-->        
        <a href="<?php echo base_url('admin/member/delete/'.$list['member_id']);?>" class="btn btn-danger" onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
       
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>
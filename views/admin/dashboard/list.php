<div class="col-md-4 col-sm-12 col-xs-12">                       
<div class="panel panel-primary text-center no-boder bg-color-gray">
    <div class="panel-body">
        <i class="fa fa-pencil fa-5x"></i>
        <h3><?php echo $blogs ?> Post </h3>
    </div>
    <div class="panel-footer back-footer-white putih">
    <a href="<?php echo base_url()?>admin/blogs">Management Blogs</a>
    </div></div></div>

<div class="col-md-4 col-sm-12 col-xs-12">                       
<div class="panel panel-primary text-center no-boder bg-color-gray">
    <div class="panel-body">
        <i class="fa fa-film fa-5x"></i>
        <h3><?php echo $slider ?> Slider </h3>
    </div>
        <div class="panel-footer back-footer-white putih">
    <a href="<?php echo base_url()?>admin/galleries">Management Slider</a>
    </div></div></div>  

<div class="col-md-4 col-sm-12 col-xs-12">                       
<div class="panel panel-primary text-center no-boder bg-color-gray">
    <div class="panel-body">
        <i class="fa fa-image fa-5x"></i>
        <h3><?php echo $galleries ?> Galleries </h3>
    </div>
        <div class="panel-footer back-footer-white putih">
    <a href="<?php echo base_url()?>admin/galleries">Management Galleries</a>
    </div></div></div>  
       

<div class="col-md-4 col-sm-12 col-xs-12">                       
<div class="panel panel-primary text-center no-boder bg-color-gray">
    <div class="panel-body">
        <i class="fa fa-users fa-5x"></i>
        <h3><?php echo $member ?> Member </h3>
    </div>
    <div class="panel-footer back-footer-white putih">
    <a href="<?php echo base_url()?>admin/member">Management Member</a>
    </div></div></div>  

<div class="col-md-4 col-sm-12 col-xs-12">                       
<div class="panel panel-primary text-center no-boder bg-color-gray">
    <div class="panel-body">
        <i class="fa fa-user fa-5x"></i>
        <h3><?php echo $admins ?> Admins </h3>
    </div>
    <div class="panel-footer back-footer-white putih">
    <a href="<?php echo base_url()?>admin/user_admin">Management Admin</a>
    </div></div></div>         

<div class="col-md-4 col-sm-12 col-xs-12">                       
<div class="panel panel-primary text-center no-boder bg-color-gray">
    <div class="panel-body">
        <i class="fa fa-list fa-5x"></i>
        <h3><?php echo $contacts ?> Inbox </h3>
    </div>
    <div class="panel-footer back-footer-white putih">
    <a href="<?php echo base_url()?>admin/contacts/inbox">Inbox Message</a>
    </div></div></div>        





